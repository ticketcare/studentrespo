/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 *  @version %I%, %G%
 */
package entity;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.ejb.embeddable.EJBContainer;
import javax.naming.NamingException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author hychen39@gmail.com
 */
public class PostFacadeTest {

    private static EJBContainer container;
    private TicketDataFacade ticketFacade;
    private PostFacade postFacade;
    private TicketCategoryFacade categoryFacade;
    final String START_DATE_STR = "01-06-2017";
    final String END_DATE_STR = "30-06-2017";

    public PostFacadeTest() {
    }

    @BeforeClass
    public static void setUpClass() {
        Map<String, Object> properties = new HashMap<String, Object>();

        properties.put(
                "org.glassfish.ejb.embedded.glassfish.configuration.file",
               "C:/glassfish4/glassfish/domains/domain1/config/domain.xml"
        );
    properties.put("installation.root", "C:/glassfish4/glassfish");
        container = EJBContainer.createEJBContainer(properties);
//        container = EJBContainer.createEJBContainer();
    }

    @AfterClass
    public static void tearDownClass() {
        container.close();
    }

    @Before
    public void setUp() throws NamingException {
        ticketFacade = (TicketDataFacade) container.getContext().lookup("java:global/classes/TicketDataFacade");
        postFacade = (PostFacade) container.getContext().lookup("java:global/classes/PostFacade");
        categoryFacade = (TicketCategoryFacade) container.getContext().lookup("java:global/classes/TicketCategoryFacade");

    }

    @After
    public void tearDown() {

    }

    /**
     * Test of findPostByTicket method, of class PostFacade.
     */
    @Test
    public void testFindPostByTicket() throws Exception {
        System.out.println("findPostByTicket");
        Ticket ticket = createTicketData();
        Post post = createPostData(ticket);

        Post expResult = post;
        Post result = postFacade.findPostByTicket(ticket);
        assertEquals(expResult, result);

        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    private Ticket createTicketData() throws ParseException {
        TicketCategoryEntity category = new TicketCategoryEntity();
        category.setName("Test-Category");
        categoryFacade.create(category);

        Ticket ticket = new Ticket();
        ticket.setName("Test Ticket 1");
        ticket.setCategory(category);
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        ticket.setStartDate(formatter.parse(START_DATE_STR));
        ticket.setStartDate(formatter.parse(END_DATE_STR));
        ticket.setQty(2);
        ticketFacade.create(ticket);

        return ticket;

    }

    private Post createPostData(Ticket ticket) throws ParseException {
        Post post = new Post();
        post.setDescription("Test-Post");
        post.setTicket(ticket);
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        post.setStartDate(formatter.parse(START_DATE_STR));
        post.setStartDate(formatter.parse(END_DATE_STR));
        postFacade.create(post);
        return post;
    }

}
