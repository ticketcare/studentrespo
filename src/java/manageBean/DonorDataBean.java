/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package manageBean;

import entity.DonorEntity;
import entity.DonorFacade;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import org.primefaces.context.RequestContext;

/**
 *
 * @author user
 */
@Named(value = "donorDataBean")
@SessionScoped
public class DonorDataBean implements Serializable {

    final static public String DONOR_ADD_PAGE = "donerData-add";
    final static public String DONER_DATA_PAGE = "donerData";
    final static public String DONOR_VIEWDETAIL_PAGE = "donerData-detail";

    enum FormMode {
        CREATE, EDIT
    }
    @EJB
    private DonorFacade donerDataFacade;
    private DonorEntity currentDonerData;
    private FormMode formMode;

    public FormMode getFormMode() {
        return formMode;
    }

    public void setFormMode(FormMode formMode) {
        this.formMode = formMode;
    }

    public static Date getCurrentDate() {
        Calendar cal = Calendar.getInstance();
        return cal.getTime();
    }

    public DonorFacade getDonerDataFacade() {
        return donerDataFacade;
    }

    public void setDonerDataFacade(DonorFacade donerDataFacade) {
        this.donerDataFacade = donerDataFacade;
    }

    public DonorEntity getCurrentDonerData() {
        return currentDonerData;
    }

    public void setCurrentDonerData(DonorEntity currentDonerData) {
        this.currentDonerData = currentDonerData;
    }

    public List<DonorEntity> getAllDonerData() {
        return donerDataFacade.findAll();
    }

    /**
     * @deprecated Use {@link #getCurrentDate() } to get the current date.
     * @return
     */
    public Date prepareCreateDate() {

        Calendar cal = Calendar.getInstance();
        return cal.getTime();
    }

    public String saveDoner() {
        if (formMode == FormMode.CREATE) {
            donerDataFacade.create(currentDonerData);
        } else {
            currentDonerData.setCreate_date(getCurrentDate());
            donerDataFacade.edit(currentDonerData);
        }
        return DONER_DATA_PAGE;
    }

    public String prepareCreate() {
        currentDonerData = new DonorEntity();
        currentDonerData.setCreate_date(getCurrentDate());
        formMode = FormMode.CREATE;
        return DONOR_ADD_PAGE;
    }

    public String prepareEdit(DonorEntity donor) {
        currentDonerData = donor;
        formMode = FormMode.EDIT;
        return DONOR_ADD_PAGE;
    }

    public void removeDonerData() {
        donerDataFacade.remove(currentDonerData);
    }

    public void removeDonerDataAction(DonorEntity donorEntity) {
        currentDonerData = donorEntity;
        RequestContext requestContext = RequestContext.getCurrentInstance();
        requestContext.execute("PF('deleteDialogVar').show();");
    }

    public String viewDonerDataAction(DonorEntity donorEntity) {
        currentDonerData = donorEntity;
        return DONOR_VIEWDETAIL_PAGE;
    }
}