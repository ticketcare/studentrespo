/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 *  @version %I%, %G%
 */
package manageBean;

import com.tcare.web.util.JsfUtil;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import entity.TicketCategoryFacade;
import entity.TicketCategoryEntity;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;

/**
 *
 * @author hychen39@gmail.com
 */
@FacesConverter("ticketCategoryConverter")
public class TicketCategoryConverter implements Converter {

    @EJB
    TicketCategoryFacade ticketCategoryFacade;

    private Logger log = Logger.getLogger(TicketCategoryConverter.class.getName());

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        Long pkValue;

        try {
            pkValue = Long.parseLong(value);
        } catch (NumberFormatException ex) {
            StringBuilder msgbuilder = new StringBuilder();
            msgbuilder.append(TicketCategoryConverter.class.getName()).append(": Value: ");
            msgbuilder.append(value);
            msgbuilder.append(". Cannot get a valid category id to find TicketCategory instance.");
            log.log(Level.WARNING, msgbuilder.toString());
            JsfUtil.addErrorMessage(msgbuilder.toString());
            return null;
        }
        return ticketCategoryFacade.find(pkValue);
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        TicketCategoryEntity currentCategory;
        try {
            currentCategory = (TicketCategoryEntity) value;
        } catch (ClassCastException exception) {
            return null;
        }
        // return the object ID.
        return currentCategory.getId().toString();
    }
}