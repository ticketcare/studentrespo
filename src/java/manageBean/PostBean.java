/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package manageBean;

import entity.PostFacade;
import java.io.Serializable;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import entity.Post;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import org.primefaces.context.RequestContext;
import entity.Ticket;
import entity.TicketDataFacade;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author user
 */
@Named(value = "postBean")
@SessionScoped
public class PostBean implements Serializable {

    @EJB
    private PostFacade postFacade;
    @EJB
    private TicketDataFacade ticketFacade;
    private Post currentPost;

    public Post getCurrentPost() {
        return currentPost;
    }

    public List<Post> getAllPosts() {
        return postFacade.findAll();
    }

    /**
     * Create a new Post Entity and persistent to database.
     *
     * @param ticket
     * @return
     */
    public String addPostDB(Ticket ticket) {
        ticket.setStatus(Ticket.Status.ON_SHELF);
        ticketFacade.edit(ticket);
        currentPost = new Post();
        currentPost.setStartDate(getCurrentDate());
        currentPost.setTicket(ticket);
        postFacade.create(currentPost);
        return null;
    }

    /**
     * 下架功能. 下架要從 ticket 找到 post. 之後 Post 的狀態改成 OFF_SHELF
     *
     * @param ticket
     * @return
     */
    public String delPostDB(Ticket ticket) throws Exception {
        //ToDo: Need to find the post by giving the ticket. Then, remove the post from the database.
        Post postInstance = postFacade.findPostByTicket(ticket);
        if (postInstance != null)
                postFacade.remove(postInstance);
        else
            throw new Exception("Cannot find Post by Given Ticket Name: " + ticket.getName());
        // update the ticket status 
        ticket.setStatus(Ticket.Status.OFF_SHELF);
        ticketFacade.edit(ticket);
        return null;
    }

    public void PostCancel() {
        currentPost = null;
    }

    public static Date getCurrentDate() {
        Calendar cal = Calendar.getInstance();
        return cal.getTime();
    }

    public Date prepareCreateDate() {
        Calendar cal = Calendar.getInstance();
        return cal.getTime();
    }
 
    public Post findByTicket(Ticket ticket){
        return postFacade.findPostByTicket(ticket);
    }
}
