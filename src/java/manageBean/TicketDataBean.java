/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package manageBean;


import entity.Ticket;
import entity.TicketDataFacade;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import manageBean.TicketDataBean.FormMode;
import org.primefaces.context.RequestContext;

/**
 *
 * @author steven
 */
@Named(value = "ticketDataBean")
@SessionScoped
public class TicketDataBean implements Serializable {

    final static public String TICKET_ADD_PAGE = "ticketData-add";
    final static public String TICKET_DATA_PAGE = "ticketData";
    final static public String TICKET_VIEWDETAIL_PAGE = "ticketData-detail";


    enum FormMode {
        CREATE, EDIT, REMOVE
    }

    @EJB
    private TicketDataFacade ticketDataFacade;
    private Ticket currentTicketData;
    private FormMode formMode;

    public FormMode getFormMode() {
        return formMode;
    }

    public void setFormMode(FormMode formMode) {
        this.formMode = formMode;
    }

    public static Date getCurrentDate() {
        Calendar cal = Calendar.getInstance();
        return cal.getTime();
    }

    public TicketDataFacade getTicketDataFacade() {
        return ticketDataFacade;
    }

    public void setTicketDataFacade(TicketDataFacade ticketDataFacade) {
        this.ticketDataFacade = ticketDataFacade;
    }

    public Ticket getCurrentTicketData() {
        return currentTicketData;
    }

    public void setCurrentTicketData(Ticket currentTicketData) {
        this.currentTicketData = currentTicketData;
    }

    public List<Ticket> getAllTickerData() {
        return ticketDataFacade.findAll();
    }

    /**
     * 請直接使用 {@link #getCurrentDate() }
     *
     * @deprecated 2017/3/23 by hychen39@gmail.com
     * @return
     */
    public Date prepareCreateDate() {
        Calendar cal = Calendar.getInstance();
        return cal.getTime();
    }

    /**
     * Save the {@link #currentTicketData} to database by calling the
     * {@link #ticketDataFacade}.
     */
    public String saveTicket() {
        if (formMode == FormMode.CREATE) {
            ticketDataFacade.create(currentTicketData);
        } else {
            currentTicketData.setCreate_date(getCurrentDate());
            ticketDataFacade.edit(currentTicketData);
        }
        return TICKET_DATA_PAGE;
    }

    public String prepareCreate() {
        currentTicketData = new Ticket();
        currentTicketData.setCreate_date(getCurrentDate());
        formMode = FormMode.CREATE;
        return TICKET_ADD_PAGE;
    }

    public String prepareEdit(Ticket ticket) {
        currentTicketData = ticket;
        formMode = FormMode.EDIT;
        return TICKET_ADD_PAGE;
    }

    public void removeTicketData() {
        ticketDataFacade.remove(currentTicketData);
        currentTicketData = null;
    }

    public void removeTicketDataAction(Ticket ticket) {
        currentTicketData = ticket;
        RequestContext requestContext = RequestContext.getCurrentInstance();
        requestContext.execute("PF('deleteDialogVar').show();");
    }

    public String viewTicketDataAction(Ticket ticket) {
        currentTicketData = ticket;
        return TICKET_VIEWDETAIL_PAGE;
    }
    
    /**
     * Prepare for posting operation.
     * Call PrimeFace dialog <code>addNewPostDialogVar</code> to show up.
     * @param ticket Ticket to post.
     */
    public void preparePosting(Ticket ticket){
        currentTicketData = ticket;
        
        RequestContext.getCurrentInstance().execute("PF('addNewPostDialogVar').show();");
        
    }
    public void prepareOffPosting(Ticket ticket){
        currentTicketData = ticket;
        RequestContext.getCurrentInstance().execute("PF('delPostDialogVar').show();");
    }
    public boolean isOnShelf(Ticket ticket){
        return ticket.getStatus().equals(Ticket.Status.ON_SHELF);
    }
    
    public boolean isOffShelf(Ticket ticket){
        return ticket.getStatus().equals(Ticket.Status.OFF_SHELF);
    }
}