/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 *  @version %I%, %G%
 */
package manageBean;

import com.tcare.web.util.JsfUtil;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import entity.DonorCategoryEntity;
import entity.DonorCategoryFacade;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;

/**
 *
 * @author hychen39@gmail.com
 */
@FacesConverter("donerCategoryConverter")
public class DonerCategoryConverter implements Converter {

    @EJB
    DonorCategoryFacade donerCategoryFacade;
    private Logger log = Logger.getLogger(DonerCategoryConverter.class.getName());

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        Long pkValue;
        try {
            pkValue = Long.parseLong(value);
        } catch (NumberFormatException ex) {
            StringBuilder msgbuilder = new StringBuilder();
            msgbuilder.append(DonerCategoryConverter.class.getName()).append(": Value: ");
            msgbuilder.append(value);
            msgbuilder.append(". Cannot get a valid category id to find TicketCategory instance.");
            log.log(Level.WARNING, msgbuilder.toString());
            JsfUtil.addErrorMessage(msgbuilder.toString());
            return null;
        }
        return donerCategoryFacade.find(pkValue);
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        DonorCategoryEntity currentCategory;
        try {
            currentCategory = (DonorCategoryEntity) value;
        } catch (ClassCastException exception) {
            return null;
        }
        // return the object ID.
        return currentCategory.getId().toString();
    }
}