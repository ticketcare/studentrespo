package entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author user
 */
@Entity
@Table(name = "Donor")
public class DonorEntity implements Serializable {

    @TableGenerator(name = "Donor", table = "SEQ_TABLE", pkColumnValue = "Donor")

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "Donor")
    private Long id;
    @Temporal(TemporalType.TIMESTAMP)
    private Date create_date;
    @OneToOne
    private DonorCategoryEntity category;
    private String donername;
    private String connectname;
    private String area_code;
    private String tel;
    private String address;
    private String content;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getCreate_date() {
        return create_date;
    }

    public void setCreate_date(Date create_date) {
        this.create_date = create_date;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public DonorCategoryEntity getCategory() {
        return category;
    }

    public void setCategory(DonorCategoryEntity category) {
        this.category = category;
    }

    public String getDonername() {
        return donername;
    }

    public void setDonername(String donername) {
        this.donername = donername;
    }

    public String getConnectname() {
        return connectname;
    }

    public void setConnectname(String connectname) {
        this.connectname = connectname;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getArea_code() {
        return area_code;
    }

    public void setArea_code(String area_code) {
        this.area_code = area_code;
    }
}